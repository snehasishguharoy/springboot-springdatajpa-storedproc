package demo.spring.boot.stored.procedure.storedprocedurespringboot.repo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EmpRepo {

	@Autowired
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<String> findEmpFnames(String fname) {
		return em.createStoredProcedureQuery("employeeproc")
				.registerStoredProcedureParameter(1, Class.class,ParameterMode.REF_CURSOR)
				.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
				.setParameter(2,fname)
				.getResultList();
	}

}
	