package demo.spring.boot.stored.procedure.storedprocedurespringboot.cntlr;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import demo.spring.boot.stored.procedure.storedprocedurespringboot.svc.EmpSvc;

@Controller
public class EmpCntlr {

	@Autowired
	private EmpSvc svc;

//	@GetMapping("/findcustomer/{fname}")
//	public List<String> findCustomers(@PathVariable("fname") String fname) {
//
//	}

	@RequestMapping("/home")
	public String firstPage() {
		return "modifybillingtariff";
	}

	@ModelAttribute("countries")
	public void countryList(Model model) {
		List<String> firstNames = Arrays.asList("Samik", "Samrat", "Sunil");
		model.addAttribute("fnames", firstNames);
	}

	@RequestMapping(value = "/empids", headers = "Accept=*/*", method = RequestMethod.GET)
	public @ResponseBody List<String> empIds(@RequestParam(value = "fname", required = true) String fname)
			throws IllegalStateException {
		return svc.findEmpFnames(fname);
	}

}
