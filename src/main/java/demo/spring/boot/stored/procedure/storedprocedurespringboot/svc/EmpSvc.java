package demo.spring.boot.stored.procedure.storedprocedurespringboot.svc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.spring.boot.stored.procedure.storedprocedurespringboot.repo.EmpRepo;

@Service
public class EmpSvc {
	@Autowired
	private EmpRepo repo;
	
	public List<String> findEmpFnames(String fname) {
		return repo.findEmpFnames(fname);
	}
	

}
