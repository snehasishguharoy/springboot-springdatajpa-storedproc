//package demo.spring.boot.stored.procedure.storedprocedurespringboot.entity;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedStoredProcedureQueries;
//import javax.persistence.NamedStoredProcedureQuery;
//import javax.persistence.ParameterMode;
//import javax.persistence.StoredProcedureParameter;
//import javax.persistence.Table;
//
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//@Entity
//@Table(name = "SAMIK.EMP")
//@Data
//@NoArgsConstructor
//@NamedStoredProcedureQueries({
//		@NamedStoredProcedureQuery(name = "getempfnames", procedureName = "employeeproc", parameters = {
//				@StoredProcedureParameter(mode = ParameterMode.IN, name = "firstname", type = String.class) }) })
//public class Employee {
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	@Column(name = "cus_id")
//	private Integer custId;
//	@Column(name = "cus_firstname")
//	private String customerfName;
//	@Column(name = "cus_surname")
//	private String customerlName;
//
//}
