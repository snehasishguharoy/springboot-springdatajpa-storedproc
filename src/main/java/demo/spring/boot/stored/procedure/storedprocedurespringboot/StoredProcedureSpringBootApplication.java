package demo.spring.boot.stored.procedure.storedprocedurespringboot;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.WebApplicationInitializer;

@SpringBootApplication
public class StoredProcedureSpringBootApplication extends SpringBootServletInitializer
		{

	public static void main(String[] args) {
		SpringApplication.run(StoredProcedureSpringBootApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(StoredProcedureSpringBootApplication.class);
	}
	

}
