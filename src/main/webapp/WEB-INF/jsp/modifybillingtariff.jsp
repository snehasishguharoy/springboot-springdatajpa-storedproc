<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP Page</title>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
</head>

<script>
	$(document)
			.ready(
					function() {
						$('#employeefirstnameid')
								.change(
										function() {
													$.getJSON(
															'empids',
															{
																fname : $(
																		this)
																		.val(),
																ajax : 'true'
															},
															function(data) {
																console
																		.log(data);
																var html = '<option value="">----Select State----</option>';
																var len = data.length;

																for (var i = 0; i < len; i++) {
																	html += '<option value="' + data[i] + '">'
																			+ data[i]
																			+ '</option>';
																}
																html += '</option>';

																$('#employeeid')
																		.html(
																				html);
															});
										});

					});
</script>
<body>

	<h3>Countries</h3>
	<form>
		<table class="data">

			<tr>
				<td>Country-Name</td>
				<td><select id="employeefirstnameid" name="employeefirstname">
						<option value="">Select Employee First Names</option>
						<c:forEach items="${fnames}" var="firstName">
							<option value="${firstName}">${firstName}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td>Employee Ids</td>
				<td><select id="employeeid" name="empid">
						<option value="">Select Employee Id</option>
				</select></td>
			</tr>
		</table>
		<input type="SUBMIT" value="Submit" />
	</form>

</body>



</html>
